
import tensorflow as tf

a = tf.constant(2)

b = tf.constant(4)

x = tf.add(a,b)

with tf.Session() as sess:
    print(sess.run(x))

    writer = tf.summary.FileWriter("./graphs", sess.graph)

    print(sess.run(x))

writer.close()