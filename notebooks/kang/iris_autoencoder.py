from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

P = 4


x = np.genfromtxt("iris.data.txt", delimiter=",")[:,0:4]

# normalization
for i in range(4):
    x[:,i] = (x[:,i]-x[:,i].min()) / x[:,i].ptp()

datax = x

# Model parameters
W1 = tf.Variable(tf.random_normal([P,P], 0, 0.1))
b1 = tf.Variable(tf.zeros([1,P]))

W2 = tf.Variable(tf.random_normal([P,2], 0, 0.1))    #the code? dim(h) = 2
b2 = tf.Variable(tf.zeros([1,2]))

W3 = tf.Variable(tf.random_normal([2,P], 0, 0.1))
b3 = tf.Variable(tf.zeros([1,P]))

W4 = tf.Variable(tf.random_normal([P,P], 0, 0.1))
b4 = tf.Variable(tf.zeros([1,P]))

# Model input and output
x = tf.placeholder(tf.float32, [None, P])
y = tf.placeholder(tf.float32, [None, P])

h1 = tf.nn.sigmoid(tf.matmul(x, W1) + b1)
h2 = tf.nn.sigmoid(tf.matmul(h1, W2) + b2)
h3 = tf.nn.sigmoid(tf.matmul(h2, W3) + b3)
r =  tf.nn.sigmoid(tf.matmul(h3, W4) + b4)

# loss
loss = tf.reduce_sum(tf.square(r - y)) # sum of the squares

# optimizer
optimizer = tf.train.AdamOptimizer(0.01)
train = optimizer.minimize(loss)


# training loop
init = tf.global_variables_initializer()
sess = tf.Session()

sess.run(init)

print("Loading what was previously trained.")
tf.train.Saver().restore(sess, "./iris_auto.ckpt")

for i in range(50000):
  sess.run(train, {x:datax, y:datax})
  #print(sess.run(h, {x:datax}))
  print(sess.run(loss, {x:datax, y:datax}))

tf.train.Saver().save(sess, "./iris_auto.ckpt")


code = sess.run(h2, {x:datax})

plt.plot(code[0:50,1], code[0:50,0], "ro")
plt.plot(code[50:100,1], code[50:100,0], "bo")
plt.plot(code[100:150,1], code[100:150,0], "go")
plt.show()

# reconstruction error for nonlinear autoencoder: < 1.2
