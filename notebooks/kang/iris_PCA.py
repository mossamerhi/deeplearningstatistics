from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

N = 120
P = 4

x = np.genfromtxt("iris.data.txt", delimiter=",")[:,0:4]

# normalization
for i in range(4):
    x[:,i] = (x[:,i]-x[:,i].min()) / x[:,i].ptp()

datax = x

# Model parameters
W1 = tf.Variable(tf.random_normal([P,2], 0, 0.1))
b1 = tf.Variable(tf.zeros([1,2]))

W2 = tf.Variable(tf.random_normal([2,P], 0, 0.1))
b2 = tf.Variable(tf.zeros([1,P]))

# Model input and output
x = tf.placeholder(tf.float32, [None, P])
y = tf.placeholder(tf.float32, [None, P])

h = (tf.matmul(x, W1) + b1)
r = (tf.matmul(h, W2) + b2)

# loss
loss = tf.reduce_sum(tf.square(r - y)) # sum of the squares

# optimizer
optimizer = tf.train.AdamOptimizer(0.005)
train = optimizer.minimize(loss)


# training loop
init = tf.global_variables_initializer()
sess = tf.Session()

sess.run(init) 

for i in range(5000):
  sess.run(train, {x:datax, y:datax})
  #print(sess.run(h, {x:datax}))
  print(sess.run(loss, {x:datax, y:datax}))

tf.train.Saver().save(sess, "./iris_PCA.ckpt")

#tf.train.Saver().restore(sess, "./iris_PCA.ckpt")

code = sess.run(h, {x:datax})
print(code)
plt.plot(code[0:50,1], code[0:50,0], "ro",label="batch^1")
plt.plot(code[50:100,1], code[50:100,0], "bo", label="batch^2")
plt.plot(code[100:150,1], code[100:150,0], "go", label="batch^3")

plt.legend(loc='best')
plt.show()

# reconstruction error for PCA: 1.69721
