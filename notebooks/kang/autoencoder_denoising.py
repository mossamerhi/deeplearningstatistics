from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

# Model parameters
W1 = tf.Variable(tf.random_normal([2,10], 0, 0.1))
b1 = tf.Variable(tf.zeros([1,10]))

W2 = tf.Variable(tf.random_normal([10,2], 0, 0.1))
b2 = tf.Variable(tf.zeros([1,2]))

W3 = tf.Variable(tf.random_normal([2,10], 0, 0.1))
b3 = tf.Variable(tf.zeros([1,10]))

W4 = tf.Variable(tf.random_normal([10,2], 0, 0.1))
b4 = tf.Variable(tf.zeros([1,2]))

# Model input and output
x = tf.placeholder(tf.float32, [None, 2])
y = tf.placeholder(tf.float32, [None, 2])

# hidden layers
h1 = tf.nn.sigmoid(tf.matmul(x, W1) + b1)
h2 = tf.nn.sigmoid(tf.matmul(h1, W2) + b2)
h3 = tf.nn.sigmoid(tf.matmul(h2, W3) + b3)

# reconstruction
r = tf.nn.sigmoid(tf.matmul(h3, W4) + b4)

# loss
loss = tf.reduce_sum(tf.square(r - y)) # sum of the squares

# optimizer
optimizer = tf.train.AdamOptimizer(0.005)
train = optimizer.minimize(loss)

# training data
# data are simulated concentrating around a quadratic curve
ori_x = np.hstack([np.random.uniform(-2, 0, 300), np.random.uniform(1,2,500), np.random.uniform(2.5,3,200)])
ori_y = ori_x * ori_x - 1
original_data = np.vstack((ori_x, ori_y)).T + np.random.multivariate_normal([0,0], [[0.005,0],[0,0.005]], 1000)

# normalize 0 - 1
original_data[:,0] = (original_data[:,0] - original_data[:,0].min()) / original_data[:,0].ptp()
original_data[:,1] = (original_data[:,1] - original_data[:,1].min()) / original_data[:,1].ptp()

# add corruption, gaussian noise
corruption_noise = np.random.multivariate_normal([0,0], [[0.01,0],[0,0.01]], 1000)
y_train = original_data
x_train = original_data + corruption_noise

# training loop
init = tf.global_variables_initializer()
sess = tf.Session()

sess.run(init)

# for i in range(20000):
#   sess.run(train, {x:x_train, y:y_train})
#   print(sess.run(loss, {x:x_train, y:y_train}))
#
#tf.train.Saver().save(sess, "./test.ckpt")
tf.train.Saver().restore(sess, "./test.ckpt")

# set up mesh grids to plot the vector field
mx = np.linspace(0,1,20)
mesh = np.vstack(np.meshgrid(mx,mx)).reshape(2,-1).T

[code2, r1] = sess.run([h2,r], {x:x_train})
r2 = sess.run(r, {x:mesh})

# plot the code as blue, reconstruction as red, corrupted data as green
plt.figure()
plt.plot(code2[:,0], code2[:,1], "bo", r1[:,0], r1[:,1], "ro", x_train[:,0], x_train[:,1], "g^")
plt.show()

# plot vector field
# vectors are scaled for graphical reason
plt.figure()
ars = r2-mesh
plt.plot(y_train[:,0], y_train[:,1], "bo")
for i in range(0,400):
    plt.arrow(mesh[i,0], mesh[i,1], ars[i,0]/4.0, ars[i,1]/4.0)
plt.show()


