from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt


#TODO: is it useful here?
def variable_summaries(var):
  """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
  with tf.name_scope('summaries'):
    mean = tf.reduce_mean(var)
    tf.summary.scalar('mean', mean)
    with tf.name_scope('stddev'):
      stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
    tf.summary.scalar('stddev', stddev)
    tf.summary.scalar('max', tf.reduce_max(var))
    tf.summary.scalar('min', tf.reduce_min(var))
    tf.summary.histogram('histogram', var)

# Model parameters
W1 = tf.Variable(tf.random_normal([2,10], 0, 0.1), name="weights")
b1 = tf.Variable(tf.zeros([1,10]), name="biases")

W2 = tf.Variable(tf.random_normal([10,1], 0, 0.1), name="weights")
b2 = tf.Variable(tf.zeros([1,1]), name="biases")

W3 = tf.Variable(tf.random_normal([1,10], 0, 0.1), name="weights")
b3 = tf.Variable(tf.zeros([1,10]), name="biases")

W4 = tf.Variable(tf.random_normal([10,2], 0, 0.1), name="weights")
b4 = tf.Variable(tf.zeros([1,2]), name="biases")

# Model input and output
x = tf.placeholder(tf.float32, [None, 2], name="data")
y = tf.placeholder(tf.float32, [None, 2], name="labels")

# hidden layers
# h2 is the one-dimensional "code"
#TODO: Use ReLU
h1 = tf.nn.sigmoid(tf.matmul(x, W1) + b1, )
h2 = tf.nn.sigmoid(tf.matmul(h1, W2) + b2, name="code")
h3 = tf.nn.sigmoid(tf.matmul(h2, W3) + b3)

# reconstruction
#TODO: Use ReLU
r = tf.nn.sigmoid(tf.matmul(h3, W4) + b4, name="reconstructed_data")

# loss
loss = tf.reduce_sum(tf.square(r - y))  # sum of the squares
tf.summary.scalar("loss", loss)


# optimizer
optimizer = tf.train.AdamOptimizer(0.02)
train = optimizer.minimize(loss)

# training data
# data are simulated concentrating around a quadratic curve
ori_x = np.hstack([np.random.uniform(-2, 0, 300), np.random.uniform(1,2,500), np.random.uniform(2.5,3,200)])
ori_y = ori_x * ori_x - 1
original_data = np.vstack((ori_x, ori_y)).T + np.random.multivariate_normal([0,0], [[0.005,0],[0,0.005]], 1000)

# normalize 0 - 1
original_data[:,0] = (original_data[:,0] - original_data[:,0].min()) / original_data[:,0].ptp()
original_data[:,1] = (original_data[:,1] - original_data[:,1].min()) / original_data[:,1].ptp()

x_train = original_data

# training loop
init = tf.global_variables_initializer()
sess = tf.Session()


sess.run(init)


#Tensorboard file
file_writer = tf.summary.FileWriter("./logs", sess.graph)

summary_op = tf.summary.merge_all()

for i in range(10000):
    _, batch_summary = sess.run([train, summary_op], feed_dict={x:x_train, y:x_train})

    file_writer.add_summary(batch_summary,i)

    if i%500:
        print(sess.run(loss, {x:x_train, y:x_train}))

print("Script complete")

tf.train.Saver().save(sess, ".logs/audoencoder.ckpt")
#tf.train.Saver().restore(sess, ".logs/audoencoder.ckpt")


# compute the one-dimensional code
[code2, r1] = sess.run([h2,r], {x:x_train})

# plot the code as blue, original data as green, reconstruction as red
plt.figure()
plt.plot(code2, np.zeros(1000)-0.3, "bo", label="code^2")
plt.plot(r1[:,0], r1[:,1], "ro", label="reconstr data")
plt.plot(x_train[:,0], x_train[:,1], "g^",label = "original data")

plt.legend(loc="best")

plt.show()


