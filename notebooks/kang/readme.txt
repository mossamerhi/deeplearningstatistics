
author: KANG

Attached are some experiments for autoencoders.

autoencoder.py: tries to extract a one-dimensional manifold using simulated data.
autoencoder_denoising.py: the denoising autoencoder. The code has equal dimensions as the data.
iris_PCA.py: run ordinary PCA using an autoencoder way.
iris_autoencoder.py: run autoencoder using nonlinear neural networks. The reconstruction error is smaller than PCA.
cancer.py: read cancer data and do PCA. This is very slow to run due to large data size.
The other files are csv data.

