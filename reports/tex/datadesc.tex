% !TeX root = main.tex
% !TeX spellcheck = en_GB

\section{Data}

In this project we will make use of two types of dataset. For the initial literature study of autoencoders, we will use simulated data, and then move on to real-world data. 

An application of autoencoders is to model the intrinsic geometry of the given dataset. There are a lot of such scenarios where autoencoders can be used. One that we will explore is data distributed according to a bimodal distribution. We will also look at data with multiple components, and see if autoencoders can be used to distinguish between the components. 


\subsection{Simulated data}
The simulated dataset is chosen such that it is easy to simulate, and such that it exhibits non-linearity in some sense. First we take a very simple bimodal distributions, the normal distribution in euclidean space, and then we look at the normal distribution on the sphere. 

\subsection{Gaussian Mixture Distribution}
Let weights $ \lambda_i > 0 $ and $ X_i \sim  \mathcal N(\mu_i, \sigma_i^2)   $, and assume $\set{X_i}_{i=1}^{N}$ is a set of mutually independent random variables
\begin{align}
y \sim \sum\limits_{i=1}^{N} \lambda_i X_i,
\end{align}
and $ \sum_i \lambda_i = 1 $. We delve into the simple case with one weight $ \alpha > 0 $:
\begin{align}
y \sim \alpha X_1 + (1-\alpha)X_2.
\end{align}
This means that with probability $ \alpha $, $ y $ is sample from $ X_1 $. If not, it is a sample from $ X_2 $. Hence for the general case, we have $ N $ components, and we first have to determine which one we sampled using a multinomial distribution. 

\begin{example}
	
\begin{figure}
	\begin{subfigure}[b]{.49\linewidth}
		\centering
		\includegraphics[width=\textwidth]{../figures/scaled/mixture_1d_two_components_separated_but_close-crop}
		\caption{$ 0.5\mathcal N(−1, 1) + 0.5\mathcal N(1, 1) $}
		\label{fig:mixture1dtwocomponentsseparatedbutclose-crop}
	\end{subfigure}
	\begin{subfigure}[b]{.49\linewidth}
		\centering
		\includegraphics[width=\textwidth]{../figures/unscaled/mixture_1d_three_components-crop}
		\caption{
			$ 0.5 \mathcal N (-10, 1) + 0.3 \mathcal N (10, 2^2) + 0.2 \mathcal N (0, 3^{2}) $}
		\label{fig:mixture1dthreecomponents-crop}
	\end{subfigure}
	\caption{The \ref{fig:mixture1dtwocomponentsseparatedbutclose-crop} is scaled to the $ \left[0, 1\right] $, while 
		\ref{fig:mixture1dthreecomponents-crop} is not scaled.}
	\label{fig:mixture:1d:two_examples}
\end{figure}
	
\end{example}



\subsubsection{Normal distribution on the sphere}
A useful parametrisation of the covariance matrix is given by $ \theta \in \R $ and $ \sigma_1^2,\sigma_2^2>0 $, then the covariance matrix
\begin{align}
	\mat{M_{\theta;\sigma_{1}^{2},\sigma_{2}^{2}}} = \mat R_{\theta}
	\begin{pmatrix}
	\sigma_1^2 & 0 \\
	0 & \sigma_2^2
	\end{pmatrix} \mat R_{\theta}^{T},
\end{align}
where $ \mat R $ denotes the rotation matrix $ 	\begin{pmatrix}
\cos\theta & \sin\theta\\
-\sin\theta&\cos\theta
\end{pmatrix} $.

\nomenclature{$ \mat M $}{Covariance matrix for the normal distribution}
\nomenclature{$ \mat{M_{\theta;\sigma_{1}^{2},\sigma_{2}^{2}}}$}{Rotation matrix with counter-clockwise rotation in $ \theta $ degrees.}

\begin{example}
	\label{dataset:2d_3components}
A fairly complicated dataset: Generated from three components with non-trivial covariances, see \autoref{fig:mixture2dthreecomponents-crop}. This data is not linearly separable, i.e. simple perceptron classifier is not sufficient at all here. 
 
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../figures/unscaled/mixture_2d_three_components_scatter-crop}
	\caption{Three components in two dimensions, the distribution is described in \autoref{eq:threecomponent_2d}}
	\label{fig:mixture2dthreecomponents-crop}
\end{figure}
	
	
	\begin{multline}
		0.2 \mathcal N \left( \begin{pmatrix}
		1\\
		1
		\end{pmatrix}, 
		\begin{pmatrix}
		0.5^{2} & 0 \\
		0 & 1.6^{2}
		\end{pmatrix}
		\right) +
%		\qquad
		0.1 \mathcal N \left( \begin{pmatrix}
		7.5\\
		10
		\end{pmatrix}, 
		\begin{pmatrix}
		0.5^{2} & 0 \\
		0 & 1.6^{2}
		\end{pmatrix}
		\right) +
		0.7 \mathcal N \left( \begin{pmatrix}
		15\\
		0.5
		\end{pmatrix},\mat M_{-45; 0.25^{2}, 0.9^{2}}
		\right)	\label{eq:threecomponent_2d}
	\end{multline}

\end{example}

\begin{example}
	\label{dataset:quadratic_curve}
	Linear PCA is really bad with non-linear dataset. For autoencoders we will sample a dataset from the following procedure:
	
	\begin{enumerate}
		\item Let $ \tilde y \sim 0.3 \mathcal U (-2,0) + 0.5 \mathcal U (1, 2) + 0.2 \mathcal U(2.5,3) $
		\item Let $ X_{\epsilon} = \mathcal N \left(
		\begin{pmatrix}
		0\\0
		\end{pmatrix}, \begin{pmatrix}
		0.005&0\\ 0 & 0.005
		\end{pmatrix}
		\right) $
	\end{enumerate}
Then the final sample is
\begin{align}
	\begin{pmatrix}
	\tilde y\\
	\tilde y^2 - 1
	\end{pmatrix} + X_{\epsilon}\label{eq:quadratic_curve} 
\end{align}
\end{example}


Overview of the dataset considered in this project shown on \autoref{tab:dataset_overview}.

\begin{table}[H]
	\centering
\begin{tabular}{l|c}
	\hline
	one-dimensional, two components&$  0.95 \mathcal N(0, 1) + 0.05 
	\mathcal N(0, 100) $\\
	&$ 0.75 \mathcal N(0, 1) + 0.25\mathcal N(1.5, 2^2) $\\
	&$ 0.5\mathcal N(−1, 1) + 0.5\mathcal N(1, 1) $\\
	&$ 0.5\mathcal N(−1, 0.5^2) + 0.5\mathcal N(1, 0.5^2) $\\
	\hline 
	two-dimensional, 3 components &  See \autoref{eq:threecomponent_2d} and \autoref{eq:quadratic_curve} \\ 
	\hline 
	high dimensional, empirical & iris dataset  \\ 
	\hline 
\end{tabular} 
\caption{All the data simulated and considered throughout the project. }
\label{tab:dataset_overview}
\end{table}


\nomenclature{$ \mathcal N(\mu,\sigma^2) $}{Normal distribution with mean $ \mu $ and variance $ \sigma^2 $}
\nomenclature{$ \mathcal U (a, b) $}{Uniform distribution on the interval $ \left[a,b\right] $}