% !TeX spellcheck = en_US
% !TeX root = main.tex

\section{Autoencoders}

%TODO: write an introduction! 

\subsection{Generally about feedforward neural networks}
Autoencoders is resemble convolutional neural networks. However, since autoencoders are not used to solve a classification or regression problem, then there will be many things different. 

\usetikzlibrary{graphs, graphdrawing, arrows.meta, quotes}

\begin{figure}[h]
	\centering
	
	\tikz \graph [math nodes, nodes={draw,circle}] {
		"x" -> h[y=1] -> y [y=1] --["loss criteria"] x [x=1.8,y=1]
	};
	
	\caption{The simplest graph of a feedforward neural network for training autoencoder-model. }
	\label{fig:graph:simple}
\end{figure}

Deep neural networks are represented by graphs, e.g. see \autoref{fig:graph:simple}. Every arrow in such a graph represents the transformation from one layer to another. This is also called the activation. See \autoref{fig:plotactivationfunctions} for examples. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{../figures/plot_activation_functions-crop}
	\caption{Plot of sigmoid activation, rectified linear units (ReLU). The identity is known as linear activation. }
	\label{fig:plotactivationfunctions}
\end{figure}


For each activation, the previous layer is carried over by an affine map, composite of weights and biases. The weights is represented by a matrix and the bias by a vector. The dimensions are determined by network architecture choices. The previous layer's dimension determine one dimension of the weight matrix, and the chosen number of outputs for the given layer, determines the other dimension. 

Every layer has then weights and biases parameters that need to be found, and in the case of non-linear activations, they need to be estimated. 

Before going further, let $ g $ an activation function. Following \cref{fig:graph:simple}, the graph has one hidden layer, and its activation is
\begin{align}
g\left(
\mat W x + \vec b
\right),
\end{align}
and now we have the output layer. Its activation function has to take the last hidden layer, and map it to an output that the deep neural network is supposed to model. Hence if the output is between zero and one, we use sigmoid as activation function. If the output is a class label, i.e. a number of distinct values, we use the softmax. For this example, we assume that $ g $ maps to the output space, and hence we arrive that:
\begin{align}
g \left(
\mat W^{\prime}
g\left(
\mat W x + \vec b
\right)
+ \vec b^{\prime}
\right). \label{eq:simple:output}
\end{align}
This the expression is the model that we have to fit. Its parameters are $ \mat W, \mat W', \vec b, $ and $ \vec b' $. These parameters are found by solving an optimization problem usually numerically. First a loss-function is chosen. Typically, there are two loss-functions used, the $ L^1 $ and the $ L^2 $. The former yields many zero weights, and the latter tend to yield weights close to zero. 

There are no guarantees that a solution is exist, or that it is unique. A fairly complicated neural network, and sparse data, may yield a problematic set of parameter estimations. 

\begin{remark}
	About the numerics: There are many pitfalls that can make numerical procedure for estimating the parameters intractable. One is the initial values for the parameters. Do not initialize all the parameters to zero! 
\end{remark}

For optimization problems a gradient based approach is the way to go. But again, this is not a surefire approach to these problems. Fine tuning hyperparameters is a big deal of finding the right set of approximated parameters. As such, for any gradient based numerical procedure, there is a step-size parameter, that enters the model as a hyperparameter.

It is due to the optimization problem aspect of training neural networks, that we need to describe the network in terms of computational graphs. When the modelled is described like that, it gives rise to automatic differentiation procedures such as back-propagation, which is what is used to find the parameters of the neural network. 

\subsection{Autoencoders}
The input layer is what is to be encoded, i.e. the features. The intermediary layers (usually denoted hidden layers in CNN) are the encoder, code and decoder. The code is known as the latent variables or latent representation. And last, the output layer is the decoded features, i.e. the reconstructed data. See \autoref{fig:autoencoder:abstract} for the general structure of an autoencoder model. The encoder and decoder may consist of many layers, while the code will be one layer. The code must be chosen to have a lower dimension (output dimension) than the input. 

The encoder and decoder should reassemble each other, but reversed; meaning the same number of encoding layers as decoding layers. 

\begin{remark}
	Regularization plays a big role in combating overfitting in neural networks. Dropout is one such strategy, where in each training-step, there is a probability of dropping a node or more from each layer. This is not employed here due to encoder/decoder restriction. Instead for autoencoders, the regularization strategies that should be tried out are
	\begin{enumerate}
		\item Lower dimensional code layer
		\item Corrupt the input data, see \autoref{sec:corruption}
		\item Sparse hidden layers\footnote{Sparse hidden layers is when you have many connections between input and the hidden layer, but where the weights are fixed to zero. }
		\item  Penalizing the derivatives by adding $ \lambda \norm{\nabla_x f(x)}^{2} $ to the loss-criteria
	\end{enumerate}	
\end{remark}

Finally, the neural network will be trained using the distance between input and reconstruction. Meaning, the decoder layer must output in the same space as the input space. This can be ensured as follows: For the pre-training normalization of the data, we may scale such that it is contained in a zero to one range. Thus we just have to ensure that the last layer in the decoder has sigmoid as activation function. 

\usetikzlibrary{graphs, graphdrawing, arrows.meta, quotes}
\usegdlibrary{layered}
\begin{figure}
	\centering
	\tikz \graph [layered layout, 
				  nodes={draw,circle}, 
				  edge quotes={auto, blue}, 
				  grow=east, 
				  orient ]
	{
		input -> encoder -> code -> decoder -> reconstruction;
		input -- reconstruction ;  
	};
	\caption{A typical layout of an autoencoder deep neural network.}
	\label{fig:autoencoder:abstract}
\end{figure}


%TODO: move me!
%relating PCA to autoencoders
Linear PCA is an autoencoder. Let $ \mat X $ denote the data. Choose the identity as activation map, and use the architecture depicted in \autoref{fig:graph:simple}. Fixing biases to zero, and fixing $ \mat W = \mat W' $, then the code $ \vec h = \mat X \mat W $ and the projected data (the reconstruction) is $ \vec h \mat W ^{T} $.


\subsection{Regularization by corruption} \label{sec:corruption}
Assuming that the dataset $ D $ has zero mean and variance 1. Corruption works by taking one data sample or the entire dataset, and adding noise to it (e.g. $ \mathcal N(0, 0.1) $ or standard normal). The loss-criteria is thus the distance between reconstructed data and the original dataset, as depicted in \autoref{fig:corruption:diagram}. This means that any constructed autoencoder model can be used to yield a Denoising Autoencoder.


\usetikzlibrary{graphs, graphdrawing, arrows.meta,quotes}
\usegdlibrary{layered}
\begin{figure}[H]
	\centering
	
	\tikz \graph [layered layout, nodes={draw,circle}, edge quotes={near start, blue}, grow=east, orient]
	{
		original data -- corrupted data -> AE layers -> reconstruction;
		original data -- reconstruction ;  
	};
	\caption{A diagram showing how Denoising Autoencoder works. The corruption is a hyperparameter in the autoencoder model.  }
	\label{fig:corruption:diagram}
\end{figure}


\subsection{Autoencoders -- where do they fit?}
\label{subsection:autoencoders:regression}
A possible application of autoencoders would be to "provide" a regression model or classification model with information computed from an autoencoder. This is a feature extraction strategy. This is referred to in the literature as pretraining. 

A few advantages for this approach: Any sensible feature extraction would vastly improve the model predictive power. Autoencoders is a feature extraction strategy that is not unsupervised, as the autoencoder does not rely on the labels of the data at all. It is also fairly autonomous in terms of building the right autoencoder for the given dataset, in the sense that, we want an autoencoder that preserves geometric and statistical properties that the original dataset admit.   

Once a code layer is generated, it should be inserted (and fixed) in the first layers of the regression classifier. This is because the code layer contains the new features that may lower the testing error of prediction models. 